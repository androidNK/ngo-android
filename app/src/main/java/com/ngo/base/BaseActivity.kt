package com.ngo.base

import android.arch.lifecycle.LifecycleOwner
import android.content.Intent
import android.os.Bundle
import android.support.annotation.DrawableRes
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import com.ngo.constants.AppConstants


/**
 * BaseActivity class
 * Created by kamran on 23/3/18.
 */
abstract class BaseActivity : AppCompatActivity(), LifecycleOwner {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutId())
        onActivityLaunch()
    }

    protected fun setupToolbar(title: String, @DrawableRes drawableRes: Int? = null) {
        val supportActionBar = supportActionBar
        supportActionBar?.title = title
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        if (drawableRes != null)
            supportActionBar?.setHomeAsUpIndicator(drawableRes)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    fun launchActivity(activityClass: Class<*>, finish: Boolean) {
        val intent = Intent(this, activityClass)
        startActivity(intent)
        if (finish)
            finish()
    }

    fun launchActivity(activityClass: Class<*>, data: String, finish: Boolean) {
        val intent = Intent(this, activityClass)
        intent.putExtra(AppConstants.INTENT_TEXT_KEY, data)
        startActivity(intent)
        if (finish)
            finish()
    }

    private fun fullScreenView() {
        window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                or View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY)
    }

    abstract fun getLayoutId(): Int
    abstract fun onActivityLaunch()
}