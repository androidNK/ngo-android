package com.ngo.utils

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.Uri
import android.util.Patterns
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.ngo.R


object AppUtil {

    fun isConnected(context: Context?): Boolean {
        val cm = context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val netInfo = cm.activeNetworkInfo
        return netInfo != null && netInfo.isConnectedOrConnecting
    }

    fun hideSoftKeypad(pActivity: Activity?) {
        if (pActivity != null && pActivity.window != null
                && pActivity.window.currentFocus != null) {
            hideSoftKeypad(pActivity, pActivity.window.currentFocus)
        }
    }

    fun hideSoftKeypad(pActivity: Activity?, view: View?) {
        val imm: InputMethodManager? = pActivity!!
                .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm?.hideSoftInputFromWindow(view!!.windowToken, 0)
    }

    fun isEmailValid(email: String): Boolean {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    fun openCallDialer(context: Context?, phoneNumber: String) {
        val intent = Intent(Intent.ACTION_DIAL)
        intent.data = Uri.parse("tel:$phoneNumber")
        context?.startActivity(intent)
    }

    fun openWhatsApp(context: Context?, whatsAppNumber: String) {
        try {
            if (AppUtil.isConnected(context)) {

                val message = "Test message from My App."
                val intent = Intent(Intent.ACTION_VIEW)
                intent.data = Uri.parse("http://api.whatsapp.com/send?phone=91$whatsAppNumber&text=$message")
                context?.startActivity(intent)
            } else {
                val sendIntent = Intent(Intent.ACTION_SENDTO, Uri.parse("smsto:91$whatsAppNumber?body="))
                sendIntent.`package` = "com.whatsapp"
                context?.startActivity(sendIntent)
            }

        } catch (e: ActivityNotFoundException) {
            openPlayStore(context, "com.whatsapp")
        }
    }

    fun sendEmail(context: Context?, requestEmail: String, number: String, message: String) {
        try {
            val intent = Intent(Intent.ACTION_SEND)
            val recipients = arrayOf(context?.getString(R.string.doctor_email))
            intent.putExtra(Intent.EXTRA_EMAIL, recipients)
            intent.putExtra(Intent.EXTRA_SUBJECT, "Camp request from- $number")
            intent.putExtra(Intent.EXTRA_TEXT, "$message\n\n\nContact me-$requestEmail")
            intent.type = "message/rfc822"
            intent.`package` = "com.google.android.gm"
            context?.startActivity(intent)
        } catch (e: ActivityNotFoundException) {
            openPlayStore(context, "com.google.android.gm")
        }

    }

    private fun openPlayStore(context: Context?, packageName: String) {
        try {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse("market://details?id=$packageName")
            context?.startActivity(intent)
        } catch (e: Exception) {
            Toast.makeText(context, "Error/n" + e.toString(), Toast.LENGTH_SHORT).show()
        }
    }
}