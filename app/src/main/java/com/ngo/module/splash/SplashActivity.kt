package com.ngo.module.splash

import android.os.Handler
import com.ngo.R
import com.ngo.base.BaseActivity
import com.ngo.module.home.ui.HomeActivity

class SplashActivity : BaseActivity() {

    var handler: Handler? = null

    var runnable: Runnable? = null

    override fun getLayoutId(): Int {
        return R.layout.activity_splash
    }

    override fun onActivityLaunch() {
        handler = Handler()
        runnable = Runnable {
            launchActivity(HomeActivity::class.java, true)
        }

        handler?.postDelayed(runnable, 3000)
    }

    override fun onDestroy() {
        super.onDestroy()

        handler?.removeCallbacks(runnable)
    }
}

