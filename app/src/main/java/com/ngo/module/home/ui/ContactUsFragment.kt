package com.ngo.module.home.ui

import android.text.TextUtils
import android.view.View
import android.widget.Toast
import com.di.io.base.BaseFragment
import com.ngo.R
import com.ngo.utils.AppUtil
import kotlinx.android.synthetic.main.fragment_contact_us.*

class ContactUsFragment : BaseFragment(), View.OnClickListener {

    override fun getLayoutId(): Int {
        return R.layout.fragment_contact_us
    }

    override fun onFragmentLaunch(view: View) {
        submitRequest.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        val email = toEmailET.text.toString()
        if (TextUtils.isEmpty(email)) {
            Toast.makeText(context, "Please enter Email", Toast.LENGTH_LONG).show()
            return
        }

        if (!AppUtil.isEmailValid(email)) {
            Toast.makeText(context, "Invalid Email", Toast.LENGTH_LONG).show()
            return
        }

        val phoneNumber = mobileNumberET.text.toString()
        if (TextUtils.isEmpty(phoneNumber)) {
            Toast.makeText(context, "Please enter Phone number", Toast.LENGTH_LONG).show()
            return
        }

        if (phoneNumber.length < 10) {
            Toast.makeText(context, "Invalid Phone number", Toast.LENGTH_LONG).show()
            return
        }

        val description = descriptionET.text.toString()
        if (TextUtils.isEmpty(description)) {
            Toast.makeText(context, "Please describe your request.", Toast.LENGTH_LONG).show()
            return
        }

        AppUtil.sendEmail(context, email, phoneNumber, description)
        toEmailET.setText("")
        mobileNumberET.setText("")
        descriptionET.setText("")
        AppUtil.hideSoftKeypad(activity)

    }

}
