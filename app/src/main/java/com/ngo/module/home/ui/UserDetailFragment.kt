package com.ngo.module.home.ui

import android.view.View
import com.di.io.base.BaseFragment
import com.ngo.R
import com.ngo.utils.AppUtil
import kotlinx.android.synthetic.main.fragment_user_detail.*


class UserDetailFragment : BaseFragment(), View.OnClickListener {

    override fun getLayoutId(): Int {
        return R.layout.fragment_user_detail
    }

    override fun onFragmentLaunch(view: View) {
        whatsAppNumberLabel.setOnClickListener(this)
        whatsAppNumberText.setOnClickListener(this)
        phoneNumberLabel.setOnClickListener(this)
        phoneNumberText.setOnClickListener(this)
        emailLabel.setOnClickListener(this)
        emailText.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        when (view!!.id) {
            R.id.whatsAppNumberLabel, R.id.whatsAppNumberText ->
                AppUtil.openWhatsApp(context, whatsAppNumberText.text.toString())

            R.id.phoneNumberLabel, R.id.phoneNumberText ->
                AppUtil.openCallDialer(context, phoneNumberText.text.toString())

            R.id.emailLabel, R.id.emailText ->
                openEmailTab()
        }

    }

    private fun openEmailTab() {
        if (activity !is HomeActivity)
            return
        (activity as HomeActivity).changeTab(1)
    }

}
