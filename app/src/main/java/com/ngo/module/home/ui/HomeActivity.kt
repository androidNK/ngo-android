package com.ngo.module.home.ui

import com.ngo.R
import com.ngo.base.BaseActivity
import com.ngo.module.home.adapter.ViewPagerAdapter
import kotlinx.android.synthetic.main.activity_home.*


class HomeActivity : BaseActivity() {

    override fun getLayoutId(): Int {
        return R.layout.activity_home
    }

    override fun onActivityLaunch() {
        val pagerAdapter = ViewPagerAdapter(supportFragmentManager)

        val presenceFragment = UserDetailFragment()
        pagerAdapter.addFragment(presenceFragment, "Home")

        val todoFragment = ContactUsFragment()
        pagerAdapter.addFragment(todoFragment, "Contact Us")

        homeViewPager.adapter = pagerAdapter
        homeViewPager.currentItem = 0
        homeTabLayout.setupWithViewPager(homeViewPager)
    }

    fun changeTab(position: Int) {
        homeViewPager.currentItem = position
    }
}

